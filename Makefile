
# $Id: Makefile,v 1.349 2022/09/15 08:43:25 gilles Exp gilles $	

PREFIX ?= /usr

clean: clean_man

.PHONY: install man clean

man:  W/imapsync.1

clean_man:
	rm -rf W/

W/imapsync.1: imapsync
	mkdir -p W
	pod2man imapsync > W/imapsync.1

install: W/imapsync.1
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install imapsync $(DESTDIR)$(PREFIX)/bin/imapsync
	chmod 755 $(DESTDIR)$(PREFIX)/bin/imapsync
	mkdir -p $(DESTDIR)$(PREFIX)/share/man/man1
	install W/imapsync.1 $(DESTDIR)$(PREFIX)/share/man/man1/imapsync.1
	chmod 644 $(DESTDIR)$(PREFIX)/share/man/man1/imapsync.1
